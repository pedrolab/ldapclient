# -*- coding: utf-8 -*-
from flask import Flask
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect

csrf = CSRFProtect()
login_manager = LoginManager()

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
app.config['PHOTO_EXTENSIONS'] = ['png', 'jpg', 'jpeg']
#app.config['DEFAULT_PASSWORD'] = '123456'
app.secret_key = app.config['SECRET_KEY']
app.jinja_env.add_extension('jinja2.ext.loopcontrols')

csrf.init_app(app)
login_manager.init_app(app)

from ldapclient import views

if __name__ == '__main__':
    app.run()

# -*- coding: utf-8 -*-
from ldapclient import app
import random

CLASSES = {
    'top': {'name': 'Top', 'icon':'<i class="fa fa-sitemap" aria-hidden="true"></i>'},
    'organization': {'name': 'Organization', 'icon':'<i class="fa fa-cubes" aria-hidden="true"></i>'},
    'organizationalUnit': {'name': 'Unit', 'icon': '<i class="fa fa-cube" aria-hidden="true"></i>'},
    'groupOfNames': {'name': 'Group', 'icon': '<i class="fa fa-users" aria-hidden="true"></i>'},
    'inetOrgPerson': {'name': 'Person', 'icon': '<i class="fa fa-user" aria-hidden="true"></i>'},
    'simpleSecurityObject': {'name': 'Security object', 'icon': '<i class="fa fa-shield" aria-hidden="true"></i>'},
    'pwdPolicy': {'name': 'Password policy', 'icon': '<i class="fa fa-key" aria-hidden="true"></i>'},
}

LDAPATTRIBS = {
    'dc': 'Domain Component',
    'o': 'Organization',
    'ou': 'Organizational Unit',
    'cn': 'Name',
    'sn': 'Surname',
    'displayName': 'Fullname',
    'uid': 'User ID',
    'mail': 'e-mail',
    'userPassword': 'Password',
    'member': 'Members',
    'description': 'Description',
    'telephoneNumber': 'Telephone',
    'jpegPhoto': 'Photo',
}


def getNavItems(session, onelevel_entries=None):
    if 'username' not in session:
        return {'breadcrumbs':[], 'home':[], 'currentDN':''}
    
    navBarItems = explodeDN(session['currentDN'], session['baseDN'])
    
    navItems = {}  
    if session['showTree']:
        navItems['tree'] = session['tree']
    else:
        navItems['tree'] = None
        
    navItems['currentDN'] = session['currentDN']
    navItems['baseDN'] = session['baseDN']
    navItems['friendlyAttribs'] = LDAPATTRIBS
    navItems['breadcrumbs'] = list(reversed(navBarItems))
    
    pos1 = session['username'].find('=')+1
    pos2 = session['username'].find(',')
    navItems['userRDN'] = session['username'][pos1:pos2]
    navItems['home'] = session['username']
    navItems['classes'] = CLASSES
    
    return navItems


def explodeDN(dn, baseDN):
    items = dn.split(",")
    items = list(reversed(items))
    result = []
    lastItem = ''
    for item in items:
        if lastItem:
            lastItem = "%s,%s" % (item, lastItem)
        else:
            lastItem = item
        # join the last two entries into one
        if lastItem == baseDN:
            item = baseDN
            result.pop()

        result.append({'name':item, 'dn':lastItem})
    return result


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in app.config['PHOTO_EXTENSIONS']


def unpackError(error):
    print type(error)
    if type(error) is str or type(error) is unicode:
        return error
    
    result = "Error"
    if error.message:
        result = error.message['desc']
        if error.message.has_key('info'):
            return "%s, %s" % (result, error.message['info'])
    return result

def generateRandomPassword():
    return '{:08}'.format(random.randrange(10**16))
